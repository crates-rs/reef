use anyhow::Result;
#[cfg(test)]
use reef::{Command, Status};
use std::time::Duration;

#[test]
fn usage() -> Result<()> {
    let git_version = Command::new("git").arg("--version").exec()?;
    assert_eq!(Some(0), git_version.exit_code());
    let cmd = Command::collect("git", 1)?;
    assert_eq!(1, cmd.len());
    Ok(())
}

#[test]
fn timeout() -> Result<()> {
    let cmd = Command::new("git")
        .arg("ls-remote")
        .arg("https://gitlab.com/crates-rs/reef.git")
        .arg("HEAD")
        .timeout(Duration::new(0, 0))
        .exec()?;
    println!("{:#?}", cmd);
    assert_eq!(Status::TimedOut, cmd.status());
    Ok(())
}
