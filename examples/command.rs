use anyhow::Result;
use reef::Command;
use std::fs::File;
use std::io::Write;
use std::thread;
use std::time::Duration;

fn main() -> Result<()> {
    let c2 = Command::new("git")
        .arg("log")
        .arg("-1")
        .current_dir(&std::env::current_dir().unwrap())
        .exec()?;
    println!("command display: {}", c2);
    println!("\ncommand debug:\n{:#?}", c2);
    println!("\ncommand json compact: {}", serde_json::to_string(&c2)?);

    let c3 = Command::new("git")
        .arg("ls-remote")
        .arg("https://gitlab.com/crates-rs/reef.git")
        .arg("HEAD")
        .timeout(Duration::new(0, 0))
        .exec()?;
    println!("{:#?}", c3);

    let c4 = Command::new("git ls-remote https://gitlab.com/crates-rs/reef.git HEAD").exec()?;
    println!("{:#?}", c4);

    let rake_dir = std::env::temp_dir().join("reef.command.rake");
    if !rake_dir.exists() {
        std::fs::create_dir_all(&rake_dir)?;
    }
    let rakefile = rake_dir.join("rakefile.rb");
    let mut file = File::create(&rakefile)?;
    file.write_all(b"task :default do\nputs 'Hello'\n$stderr.puts 'stderr'\nraise 'failure'\nend")?;
    file.flush()?;
    thread::sleep(Duration::from_millis(500));
    let rake = Command::new("rake")
        .arg("default")
        .current_dir(&rake_dir)
        .timeout(std::time::Duration::from_secs(20 * 60))
        .exec()?;
    println!("{:#?}", rake);
    std::fs::remove_file(&rakefile)?;
    std::fs::remove_dir_all(&rake_dir)?;

    // collect recent history
    let recent = Command::collect("", 3)?;
    println!("recent:\n{:#?}", recent);

    Ok(())
}
