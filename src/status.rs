use colored::*;
use std::fmt::{Debug, Display, Formatter};

#[derive(Debug, PartialEq)]
pub enum Status {
    Unknown,
    TimedOut,
    Error,
    Ok,
}

impl Display for Status {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            Status::Ok => write!(f, "{}", "✓".green().bold()),
            Status::TimedOut => write!(f, "{}", "!".yellow().bold()),
            Status::Error => write!(f, "{}", "X".red().bold()),
            _ => write!(f, "{}", "?".yellow().bold()),
        }
    }
}
