require "raykit"

task :test do
    run "cargo test"
end

task :publish => [:test,:integrate,:tag] do
    run "cargo publish"
end

task :default => [:test,:integrate,:tag,:push] do
    puts "  #{PROJECT.name} v#{PROJECT.version}"
end