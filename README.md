reef
=====
 A Rust library to execute and log system commands.

[![Reef crate](https://img.shields.io/crates/v/reef.svg)](https://crates.io/crates/reef)
[![Reef documentation](https://docs.rs/reef/badge.svg)](https://docs.rs/reef)

### Usage

Add this to your `Cargo.toml`:

```toml
[dependencies]
reef = "0"
```

## License

Reef is distributed under the terms of both the MIT license and the
Apache License (Version 2.0). See [LICENSE-APACHE](LICENSE-APACHE) and
[LICENSE-MIT](LICENSE-MIT) for details. Opening a pull requests is
assumed to signal agreement with these licensing terms.